// import React, { useState } from "react";

export const Lable = ({ data, setAllData,boxOne }) => {


  
 const onChangeFunction = (even) => {
       
  if (even.target.checked) {
    data.isSelected = true;
  
    setAllData(boxOne);
  } else {
    data.isSelected = false;
    setAllData(boxOne);
  
  }
}

 
  
  return (
    <label>
      <input
        type="checkbox"
        
        id={data.name}
        onChange={onChangeFunction}
      />
      <h2>{data.name}</h2>
    </label>
  );
};
