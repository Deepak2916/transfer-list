import React from "react";

import { Lable } from "./Lable";
export const BoxTwo = ({ boxTwo, setAllData }) => {
  return (
    <div className="boxTwo">
      {boxTwo
        .filter((data) => {
          return data.place === "box2";
        })
        .map((data) => {
          return (
            <Lable
              key={data.name}
              data={data}
              setAllData={setAllData}
              boxOne={boxTwo}
            />
          );
        })}
    </div>
  );
};
