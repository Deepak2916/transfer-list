import React, { useState } from "react";

import { Lable } from "./Lable";

export const BoxOne = ({ boxOne, setAllData }) => {
 

  return (
    <div className="boxOne">
      {boxOne
        .filter((data) => {
          return data.place === "box1";
        })
        .map((data) => {
          return (
        
            <Lable key={data.name} data={data} setAllData={setAllData} boxOne={boxOne} />
             
              );
        }
        )}
    </div>
  );
};
