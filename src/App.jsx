import { useState } from "react";

import "./App.css";
import { Header } from "./components/Header";
import { BoxOne } from "./components/BoxOne";

import { BoxTwo } from "./components/BoxTwo";

function App() {
  const data = [
    {
      name: "Svelte",
      place: "box1",
      isSelected: false,
    },
    {
      name: "Js",
      place: "box1",
      isSelected: false,
    },
    {
      name: "HTML",
      place: "box1",
      isSelected: false,
    },
    {
      name: "CSS",
      place: "box1",
      isSelected: false,
    },
    {
      name: "Ts",
      place: "box2",
      isSelected: false,
    },
    {
      name: "React",
      place: "box2",
      isSelected: false,
    },
    {
      name: "Angular",
      place: "box2",
      isSelected: false,
    },
    {
      name: "Vue",
      place: "box2",
      isSelected: false,
    },
  ];
  const [allData, setAllData] = useState(data);

  const transferDataToRight = () => {
    const newData = allData.map((data) => {
      if (data.place === "box1") {
        data.place = "box2";
      }
      return data;
    });
    setAllData(newData);
  };

  const transferDataToLeft = () => {
    const newData = allData.map((data) => {
      if (data.place === "box2") {
        data.place = "box1";
      }
      return data;
    });
    setAllData(newData);
  };

  const transferSelectedDataToRight = () => {
    const newData = allData.map((data) => {
      if (data.isSelected && data.place == "box1") {
        data.place = "box2";
        data.isSelected = false;
      }
      return data;
    });
    setAllData(newData);
  };

  const transferSelectedDataToLeft = () => {
    const newData = allData.map((data) => {
      if (data.isSelected && data.place == "box2") {
        data.place = "box1";
        data.isSelected = false;
      }
      return data;
    });
    setAllData(newData);
  };

  return (
    <>
      <Header />
      <div className="main-container">
        <BoxOne boxOne={allData} setAllData={setAllData} />
        <div className="middle-box">
          <button className="toBoxTwo" onClick={transferDataToRight}>
          <i className="fa-solid fa-angles-right fa-lg"></i>
          </button>

          <button className="toBoxTwo" onClick={transferSelectedDataToLeft}>
          <i className="fa-solid fa-angle-left fa-lg"></i>
          </button>
          <button className="toBoxTwo" onClick={transferSelectedDataToRight}>
          <i className="fa-solid fa-angle-right fa-lg"></i>
          </button>
          <button className="toBoxTwo" onClick={transferDataToLeft}>
          <i className="fa-solid fa-angles-left fa-lg"></i>
          </button>
        </div>
        <BoxTwo boxTwo={allData} setAllData={setAllData} />
      </div>
    </>
  );
}

export default App;
